var Slider = function ( obj ) {

    var
        $sliderNode     = obj.sliderNode,
        imageContSize   = obj.imageContSize,
        imagesLinks     = obj.imagesLinks,
        selectedImage   = obj.selectedImage || 0,
        imageClassName  = obj.imageClassName,
        mayScroll       = obj.mayScroll,
        $arrowLeft      = obj.arrowLeft,
        $arrowRight     = obj.arrowRight,
        onChangeCurrentImage = obj.onChangeCurrentImage,
        onClickOnImage  = obj.onClickOnImage;

    if ( !$sliderNode || !imageContSize || !imagesLinks )
        throw new Error( '[Slider]: not enough params' );

    if ( $arrowLeft ) $arrowLeft.click(function(){ drift( -1, true ) });
    if ( $arrowRight ) $arrowRight.click(function(){ drift( 1, true ) });

    var
        imagesQuantity,
        positionDelta,
        positionPrefix,
        currentImageIndex,
        bin = [];

    var calculateParams = function () {
        var nodeWidth = $sliderNode.width();
        imagesQuantity = Math.floor( nodeWidth / imageContSize );
        if ( ! (imagesQuantity % 2) ) imagesQuantity--;
        positionDelta = Math.floor( nodeWidth / imagesQuantity );
        positionPrefix =Math.floor( ( nodeWidth - imagesQuantity * positionDelta + positionDelta - imageContSize ) / 2 );
    }

    var createBin = function () {
        var bin = [];
        for ( var i in imagesLinks ) {
            bin[i] = {};
            bin[i].obj = $('<div class="'+imageClassName+'">...</div>');
            //if ( imageClassName ) bin[i].obj.attr( 'class', imageClassName );
            //else throw new Error( '[Slider.createBin]: TODO — add me a default class.' );
            bin[i].obj.html( '<center>'+i+'</center>' );
            bin[i].obj.attr( 'id', i );
            bin[i].obj[0].onclick = function(){ onClickOnImage( this.id ) };
        }
        return bin;
    }

    var Rendered = [],
        bin = [];

    var render = function ( centerTo ) {
        for ( var i in Rendered ) { bin[Rendered[i]].obj.remove() }
        Rendered = [];
        selectedImage = ( centerTo || centerTo == 0 ) ? centerTo : selectedImage;
        currentImageIndex = selectedImage;

        var center = Math.ceil( imagesQuantity / 2 ),
            index  = selectedImage - center + 1;
        if ( index < 0 ) index = 0;
        if ( index > ( bin.length - 1 - imagesQuantity ) ) index = bin.length  - imagesQuantity;
        for ( var i = 0; i < imagesQuantity; i++ ) {
            var left = positionPrefix + positionDelta * i + 'px';
            bin[index + i].obj.css( 'left', left );
            bin[index + i].obj.appendTo( $sliderNode );
            // if ( typeof onClickOnImage == 'function' ) {
            //     bin[index + i].obj.click( (function(T){
            //     return function(e){
            //         onClickOnImage(T)}})( index + i ) );
            // }
            Rendered.push( index + i );
        }
    }
    this.render = render;

    var animatedNow = [];

    function drift( value, doAnimate, to ) {
        if ( animatedNow.length ) return;
        if ( !to && to != 0 ) {
            if ( !+value ) throw new Error('[Slider.drift]: "value" is '+value+'. WTF?');
            var delta   = value > 0 ? 1 : -1, 
                element = value > 0 ? Rendered[Rendered.length - 1] : Rendered[0];
        } else {
            if ( !+to && to != 0 ) throw new Error('[Slider.drift]: "to" is '+to+'. WTF?');
            if ( currentImageIndex == to ) return;
            console.log( 'cII: ' + currentImageIndex );
            var delta = value = ( to - currentImageIndex ) > 0 ? 1 : -1,
                element = to - Math.ceil( imagesQuantity / 2 ) * delta;
            console.log( 'to is ' + to + ', element is ' + element );
        }
        var n = 0,
            i = element;
        if ( $arrowLeft || $arrowRight ) manageArrows( delta, (element + value) );
        // append new elements to $sliderNode, add its bin-indexes to Rendered,
        // and calculate its positions
        while ( i != (element + value) && bin[i + delta] ) {
            i += delta;
            if (value > 0) {
                Rendered.push(i);
                var left = positionPrefix + positionDelta * (Rendered.length - 1) + 'px';
            }
            else {
                Rendered.unshift(i);
                n--;
                var left = positionPrefix + positionDelta * n + 'px';
            }
            bin[i].obj.css( 'left', left );
            bin[i].obj.appendTo( $sliderNode );
        }
        var elemsAdded = i - element;
        // set new positions ( animated if it is needed )
        for ( var i in Rendered ) {
            var left = value < 0 ?
                positionPrefix + positionDelta * i + 'px':
                positionPrefix + positionDelta * ( i - elemsAdded ) + 'px';
                animatedNow.push( $.Deferred() );
            if ( doAnimate ) bin[ Rendered[i] ].obj.animate( { 'left' : left }, 500, 'linear', (function(a){return function(){
                    animatedNow[a].resolve()
                }})(animatedNow.length -1) );
            else {
                bin[ Rendered[i] ].obj.css( 'left', left );
                animatedNow[animatedNow.length -1].resolve();
            }
        }
        $.when.apply( this, animatedNow ).done( function(){animatedNow = []; removeInvisible(elemsAdded)} );
    }
    this.drift = drift;

    // remove invisible elements ( old ) from $sliderNode,
    // and its indexes from Rendered
    function removeInvisible (value) {
        var i = Math.abs(value);
        while (i) {
            if ( value > 0 ) var index = Rendered.shift();
            else var index = Rendered.pop();
            bin[index].obj.remove();
            i--
        }
        var centerIndex = Math.ceil( Rendered.length / 2 ) - 1;
        currentImageIndex = Rendered[centerIndex];
        if ( typeof onChangeCurrentImage == 'function' ) onChangeCurrentImage( currentImageIndex );
    };

    function manageArrows ( direction, requestedIndex ) {
        var $forwardArrow = direction > 0 ? $arrowRight : $arrowLeft;
            $backwardArrow = direction > 0 ? $arrowLeft : $arrowRight;
        if ( ! bin[requestedIndex + direction] ) $forwardArrow.hide();
        $backwardArrow.show();
    }

    function addWheelEvent ( obj ) {
        if (obj.addEventListener) {
            if ('onwheel' in document) {
                obj.addEventListener ("wheel", scrolled, false);
            } else if ('onmousewheel' in document) {
                obj.addEventListener ("mousewheel", scrolled, false);
            } else {
                obj.addEventListener ("MozMousePixelScroll", scrolled, false);
            }
        } else {
            obj.attachEvent ("onmousewheel", scrolled);
        }
    }

    var scrolled = function ( e, doStopCount ) {
        if ( doStopCount ) {
            drift ( wheel.snaps, true );
            wheel = { snaps : 0, waiting : false };
            return;
        }
        e = e || window.event;
        var delta = e.deltaY || e.detail || e.wheelDelta;
        wheel.snaps += delta > 0 ? -1 : 1;
        if ( ! wheel.waiting ) {
            wheel.waiting = true;
            setTimeout( function(){scrolled( '', true )}, 50 )
        }
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    }

    if ( mayScroll ) {
        var wheel = { snaps : 0, waiting : false };
        addWheelEvent( $sliderNode[0] );
    }

    this.init = function () {
        bin = createBin();
        calculateParams();
        render();
    }


    this.onResize = function () {
        calculateParams();
        render();
    }
}
